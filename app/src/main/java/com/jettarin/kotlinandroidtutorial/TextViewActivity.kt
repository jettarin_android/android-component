package com.jettarin.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_text_view.*

class TextViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_view)
        val TextView = findViewById<TextView>(R.id.text_view_id)
        TextView.setOnClickListener{
            Toast.makeText(MainActivity@this, R.string.text_on_click, Toast.LENGTH_LONG).show()
        }
    }
}
